const { setHeadlessWhen } = require("@codeceptjs/configure")

// turn on headless mode when running with HEADLESS=true environment variable
// HEADLESS=true npx codecept run
setHeadlessWhen(process.env.HEADLESS)

exports.config = {
	tests: "./e2e/tests/**/*.ts",
	output: "./e2e/output",
	helpers: {
		Puppeteer: {
			url: `https://home.${process.env.FLAP_URL}`,
			show: !process.env.CI,
			getPageTimeout: 20000,
			waitForTimeout: 20000,
			waitForNavigation: "networkidle0",
			windowSize: "1920x1080",
			chrome: {
				ignoreHTTPSErrors: true,
				args: ["--no-sandbox", "--disable-dev-shm-usage"],
				...(process.profile === "chrome-ci"
					? {
						executablePath: "/usr/bin/chromium-browser",
					}
					: {}),
			},
		},
	},
	include: {
		I: "./e2e/steps_file.js",
		homePage: "./e2e/pages/home.ts",
		settingsPage: "./e2e/pages/settings.ts",
		createUserStep: "./e2e/steps/createUser.js",
	},
	bootstrap: null,
	mocha: {
		reporterOptions: {
			mochaFile: "output/result.xml",
		},
	},
	name: "home",
	plugins: {
		retryFailedStep: {
			enabled: true,
		},
		screenshotOnFail: {
			enabled: true,
		},
		pauseOnFail: {
			enabled: false,
		},
	},
}
