import * as express from "express"
import { getSetupProgress } from "../lib/setup"

// Check that a value is true
export function isTrue(value: boolean | Promise<boolean>) {
	return async function(
		_request: express.Request,
		_response: express.Response,
		next: express.NextFunction,
	) {
		if (value instanceof Promise) {
			value = await value
		}

		if (!value) {
			next({
				code: 400,
				message: new Error("This functionality is disabled."),
			})
		} else {
			next()
		}
	}
}

// Check that a value is false
export function isFalse(value: boolean | Promise<boolean>) {
	return async function(
		_request: express.Request,
		_response: express.Response,
		next: express.NextFunction,
	) {
		if (value instanceof Promise) {
			value = await value
		}

		if (value) {
			next({
				code: 400,
				message: new Error("This functionality is disabled."),
			})
		} else {
			next()
		}
	}
}

// Check that the calling user is an admin
export function isAdmin(
	request: express.Request,
	_response: express.Response,
	next: express.NextFunction,
) {
	const user = (request as any).user

	if (!user || !user.admin) {
		next({
			code: 400,
			message: new Error("You must be an administrator to do that."),
		})
	} else {
		next()
	}
}

// Check that the calling user is the same user as the one returned by getUsername
export function isUser(getUsername: (request: express.Request) => string) {
	return function(
		request: express.Request,
		_response: express.Response,
		next: express.NextFunction,
	) {
		const user = (request as any).user

		if (!user || user.username !== getUsername(request)) {
			next({
				code: 400,
				message: new Error(
					"You are not the appropriate user to do that.",
				),
			})
		} else {
			next()
		}
	}
}

// Check that the setup is ongoing
export async function isSetup(
	_request: express.Request,
	_response: express.Response,
	next: express.NextFunction,
) {
	const progress = await getSetupProgress()

	if (progress >= 2) {
		next({
			code: 400,
			message: new Error("You must be in setup mode to do that."),
		})
	} else {
		next()
	}
}

export function combineChecks(
	operator: "OR" | "AND",
	...checks: express.RequestHandler[]
) {
	return async function(
		request: express.Request,
		response: express.Response,
		next: express.NextFunction,
	) {
		let errors: any[] = []
		function fakeNext(error: any) {
			if (error !== undefined) {
				errors.push(error.message.message)
			}
		}

		// Call all checks with a fake next function
		const checksPromises = checks.map(check =>
			check(request, response, fakeNext),
		)
		// Wait for the checks to complete
		await Promise.all(checksPromises)

		switch (operator) {
			case "OR":
				// Error if all checks are errors
				if (errors.length >= checks.length) {
					return next(errors.join(" OR "))
				}
				break

			case "AND":
				// Error if a least one check is an error
				if (errors.length >= 1) {
					return next(errors.join(" AND "))
				}
				break
		}
		next()
	}
}

export function isAdminOrUser(
	getUsername: (request: express.Request) => string,
) {
	return combineChecks("OR", isAdmin, isUser(getUsername))
}

export const isAdminOrSetup = combineChecks("OR", isAdmin, isSetup)
