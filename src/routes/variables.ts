import * as express from "express"
import { getVariables, setVariables, getVariable } from "../lib"
import { isAdmin, isFalse } from "../tools"

export const variablesRouter = express.Router()

variablesRouter
	// /variables
	.route("/")
	.get(async (_request, response) => {
		response.json(await getVariables()).end()
	})
	.put(
		isFalse(
			getVariable("FLAG_DISABLE_ADVANCED_SETTINGS") as Promise<boolean>,
		),
		isAdmin,
		async (request, response, next) => {
			try {
				await setVariables(request.body)
				response.status(200)
				response.end()
			} catch (error) {
				next(error)
			}
		},
	)
