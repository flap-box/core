import * as express from "express"
import { body, validationResult, param } from "express-validator"

import { isAdminOrSetup, formatValidationErrors, isAdmin } from "../tools"
import {
	getDomains,
	getDomain,
	setDomain,
	deleteDomain,
	getPrimaryDomain,
	setPrimaryDomain,
	patchDomain,
} from "../lib"
import { DomainNameProviders } from "../front/src/models"

export const domainsRouter = express.Router()

// Middlewares
domainsRouter.param("domainId", async (request, _response, next) => {
	try {
		// Validate the userId
		await param("domainId")
			.trim()
			.isFQDN()
			.run(request)

		validationResult(request).throw()
	} catch (error) {
		return next(formatValidationErrors(error))
	}

	// Add the user object to the request
	try {
		const domain = await getDomain(request.params.domainId)
		;(request as any).targetedDomain = domain
		next()
	} catch (error) {
		next({ code: 404, message: error })
	}
})

// /domains
domainsRouter
	.route("/")
	.get(isAdminOrSetup, async (_request, response, next) => {
		try {
			response.json(await getDomains())
			response.end()
		} catch (error) {
			next(error)
		}
	})

	.post(
		isAdminOrSetup,
		body("name")
			.customSanitizer((value: string) => value.toLowerCase())
			.trim()
			.isFQDN(),
		body("provider").custom(value => {
			if (!Object.values(DomainNameProviders).includes(value)) {
				throw new Error(
					`Provider must be one of: ${Object.values(
						DomainNameProviders,
					).join(",")}`,
				)
			}

			return true
		}),
		body("authentication")
			.optional()
			.isString()
			.trim()
			.isLength({ min: 1, max: 128 })
			.not()
			.contains(" "),
		body("username")
			.optional()
			.isString()
			.trim()
			.isLength({ min: 1, max: 128 })
			.not()
			.contains(" "),
		async (request, response, next) => {
			try {
				validationResult(request).throw()
			} catch (error) {
				return next({
					code: 400,
					message: formatValidationErrors(error),
				})
			}

			try {
				await setDomain(request.body)

				response.json(await getDomain(request.body.name))
				response.end()
			} catch (error) {
				next(error)
			}
		},
	)

// /domains/:domainId
domainsRouter
	.route("/:domainId")
	.patch(
		isAdminOrSetup,
		body("name")
			.trim()
			.isFQDN(),
		body("provider").custom(value => {
			if (!Object.values(DomainNameProviders).includes(value)) {
				throw new Error(
					`Provider must be one of: ${Object.values(
						DomainNameProviders,
					).join(",")}`,
				)
			}

			return true
		}),
		body("authentication")
			.optional()
			.isString()
			.trim()
			.isLength({ min: 1, max: 128 })
			.not()
			.contains(" "),
		body("username")
			.optional()
			.isString()
			.trim()
			.isLength({ min: 1, max: 128 })
			.not()
			.contains(" "),
		async (request, response, next) => {
			try {
				validationResult(request).throw()
			} catch (error) {
				return next({
					code: 400,
					message: formatValidationErrors(error),
				})
			}

			try {
				await patchDomain(request.body)

				response.json(await getDomain(request.body.name))
				response.end()
			} catch (error) {
				next(error)
			}
		},
	)
	.delete(isAdmin, async (request, response, next) => {
		const domain = (request as any).targetedDomain

		try {
			// Prevent deleting the current domain name.
			if (domain.name === request.host) {
				throw new Error("Can't delete the current domain name.")
			}

			response.json(await deleteDomain(domain.name))
			response.end()
		} catch (error) {
			next(error)
		}
	})

// /domains/primary
domainsRouter
	.route("/primary")
	.get(isAdminOrSetup, async (_request, response, next) => {
		try {
			response.json(await getPrimaryDomain())
			response.end()
		} catch (error) {
			next(error)
		}
	})

	.put(
		isAdmin,
		body("name")
			.trim()
			.isFQDN(),
		async (request, response, next) => {
			try {
				validationResult(request).throw()
			} catch (error) {
				return next({
					code: 400,
					message: formatValidationErrors(error),
				})
			}

			try {
				await setPrimaryDomain(request.body.name)
				response.json(await getPrimaryDomain())
				response.end()
			} catch (error) {
				next(error)
			}
		},
	)
