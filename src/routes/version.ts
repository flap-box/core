import * as express from "express"

const FLAP_VERSION = process.env.FLAP_VERSION

export const versionRouter = express
	.Router()

	// /version
	.get("/", (_request, response) => {
		response.json({ version: FLAP_VERSION }).end()
	})
