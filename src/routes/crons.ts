import * as express from "express"
import { updateMailAliases } from "../lib"

export const cronsRouter = express.Router()

// /crons
cronsRouter.get("/update-mail-aliases", async (_request, response, next) => {
	try {
		await updateMailAliases()
		response.end()
	} catch (error) {
		next(error)
	}
})
