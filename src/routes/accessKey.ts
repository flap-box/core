import { promises as fs } from "fs"

import * as express from "express"
import { body, validationResult } from "express-validator"

import { readFile, isAdmin, formatValidationErrors, isFalse } from "../tools"
import { getVariable } from "../lib"

export const accessKeyRouter = express.Router()

// /accessKey
accessKeyRouter
	.route("/")
	.get(
		isFalse(
			getVariable("FLAG_DISABLE_ADVANCED_SETTINGS") as Promise<boolean>,
		),
		isAdmin,
		async (_request, response, next) => {
			try {
				response.send({
					accessKey: await readFile("/root/.ssh/authorized_keys"),
				})
				response.end()
			} catch (error) {
				next(error)
			}
		},
	)
	.put(
		isFalse(
			getVariable("FLAG_DISABLE_ADVANCED_SETTINGS") as Promise<boolean>,
		),
		isAdmin,
		body("accessKey")
			.trim()
			.isLength({ min: 0, max: 1000 }),
		async (request, response, next) => {
			// Validate values
			try {
				validationResult(request).throw()
			} catch (error) {
				return next({
					code: 400,
					message: formatValidationErrors(error),
				})
			}

			try {
				await fs.writeFile(
					"/root/.ssh/authorized_keys",
					request.body.accessKey,
				)

				response.send(await readFile("/root/.ssh/authorized_keys"))
				response.end()
			} catch (error) {
				next(error)
			}
		},
	)
