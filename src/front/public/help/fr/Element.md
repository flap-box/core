# Element

---

Element est un logiciel de messagerie instantanée qui s&#39;appuie sur le protocole matrix.

### Discutez sur tout vos appareils

Element existe aussi pour tous vos appareils. [Il vous suffit de télécharger les applications proposées à ce lien](https://element.io/get-started).

Lors de la connexion, il vous faudra indiquer le nom de domaine de votre serveur:

> `https://{{domain_name}}`

Ou, si cela ne fonctionne pas:

> `https://matrix.{{domain_name}}`

### Fonctionnalités clés

-   Communiquer sous forme de chat avec les utilisateurs internes mais aussi externes.
-   Créez des salons de discutions, pour une équipe ou un sujet.
-   Passer des appels audio et vidéo.

[Vous trouverez ici plus de fonctionnalités](https://element.io/features).
