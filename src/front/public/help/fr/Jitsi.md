# Jitsi

---

Jitsi vous permer de créer des visioconférences. Vous pouvez ensuite partager le lien de la conférence pour inviter vos collaborateurs.

### Applications

Il existe des applications Jitsi pour tous vos appareils.

-   [Pour vos smartphone, il vous suffit de télécharger les applications proposées à ce lien](https://jitsi.org/#download).
-   [Pour Linux](https://github.com/jitsi/jitsi-meet-electron/releases/download/v2.0.2/jitsi-meet-x86_64.AppImage).
-   [Pour MacOS](https://github.com/jitsi/jitsi-meet-electron/releases/download/v2.0.2/jitsi-meet.dmg).
-   [Pour Windows](https://github.com/jitsi/jitsi-meet-electron/releases/download/v2.0.2/jitsi-meet.exe).

Pour utiliser le serveur Jitsi de votre FLAP, il vous faudra indiquer le nom de domain suivant dans les paramètre de l'application:

> `https://jitsi.{{domain_name}}`
