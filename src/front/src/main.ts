import Vue from "vue"
import App from "./App.vue"

import "keen-ui/src/bootstrap"

import router from "./router"
import store from "./store"
import i18n from "./i18n"

import "./app.less"

Vue.config.productionTip = false

new Vue({
	router,
	store,
	i18n,
	render: h => h(App),
}).$mount("#app")

// Hot reloading for locales.
if (module.hot) {
	module.hot.accept(["./en"], function() {
		i18n.setLocaleMessage("en", require("./en").default)
	})
}
