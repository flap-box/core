import Vue from "vue"
import Router from "vue-router"

import Home from "./views/Home.vue"
import { apiClient } from "./apiClient"

Vue.use(Router)

export default new Router({
	mode: "history",
	base: process.env.BASE_URL,
	routes: [
		{ path: "*", redirect: "/" },
		{
			path: "/",
			name: "home",
			component: Home,
			beforeEnter: async (to, from, next) => {
				// If not connected, go to /login
				try {
					await apiClient.get("/me")
				} catch {
					return next("login")
				}

				// If the setup is no completed, go to /setup
				const response = await apiClient.get("/setup")
				if (response.data.progress !== 2) {
					return next("setup")
				}

				next()
			},
		},
		{
			path: "/settings",
			name: "settings",
			// route level code-splitting
			// this generates a separate chunk (settings.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
				import(
					/* webpackChunkName: "settings" */ "./views/Settings.vue"
				),
			beforeEnter: async (to, from, next) => {
				// If not connected, go to /login
				try {
					await apiClient.get("/me")
				} catch {
					return next("login")
				}

				// If the setup is no completed, go to /setup
				const response = await apiClient.get("/setup")
				if (response.data.progress !== 2) {
					return next("setup")
				}

				next()
			},
			children: [{ path: "/settings/:view" }],
		},
		{
			path: "/login",
			name: "login",
			beforeEnter: async (to, from, next) => {
				// If the setup is no completed, go to /setup
				const response = await apiClient.get("/setup")
				if (response.data.progress !== 2) {
					return next("setup")
				}

				// If connected, go to home
				try {
					await apiClient.get("/me")
					return next("home")
				} catch {
					next()
				}

				return ((window.location as any) = `${location.protocol}//auth.${location.host}`)
			},
		},
		{
			path: "/setup",
			name: "setup",
			// route level code-splitting
			// this generates a separate chunk (setup.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
				import(/* webpackChunkName: "setup" */ "./views/Setup.vue"),
			beforeEnter: async (to, from, next) => {
				// If the setup is completed, go to home
				const response = await apiClient.get("/setup")
				if (response.data.progress >= 2) {
					return next("home")
				}

				next()
			},
		},
	],
})
