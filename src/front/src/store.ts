import Vue from "vue"
import Vuex from "vuex"

import { apiClient } from "./apiClient"

import { Variable, IUser } from "./models"

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		user: {
			fullname: "",
			username: "",
		},
		variables: {},
		version: "",
		accessKey: "",
	},
	mutations: {
		setUser(state, user: IUser) {
			state.user = user
		},
		updateVariable(state, { name, value }) {
			const variables: { [key: string]: Variable } = state.variables

			state.variables = {
				...variables,
				...{ [name]: { ...variables[name], value } },
			}
		},
		setVariables(state, variables) {
			state.variables = variables
		},
		setVersion(state, version) {
			state.version = version
		},
		setAccessKey(state, accessKey) {
			state.accessKey = accessKey
		},
	},
	actions: {
		async getUser({ commit }) {
			const response = await apiClient.get("/me")
			commit("setUser", response.data)
		},

		async getVariables({ commit }) {
			const response = await apiClient.get("/variables")
			commit("setVariables", response.data)
		},
		async updateVariable({ commit, state }, variable) {
			commit("updateVariable", variable)
			await apiClient.put("/variables", state.variables)
		},

		async getVersion({ commit }) {
			const response = await apiClient.get("/version")
			commit("setVersion", response.data.version)
		},

		async getAccessKey({ commit }) {
			const response = await apiClient.get("/accessKey")
			commit("setAccessKey", response.data.accessKey)
		},
		async setAccessKey({ commit }, accessKey) {
			const response = await apiClient.put("/accessKey", { accessKey })
			commit("setAccessKey", response.data)
		},
	},
})
