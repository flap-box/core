export interface IUser {
	fullname: string
	username: string
	email: string
	admin: boolean
}

export enum DomainNameProviders {
	Unknown = "unknown",
	Localhost = "localhost",
	Local = "local",
	Namecheap = "namecheap",
	DuckDns = "duckdns",
	FLAP = "flap",
}

export enum DomainAuthenticationSchemes {
	None = "none",
	Token = "token",
	UsernamePassword = "username/password",
}

export const DomainNameProvidersInfo = {
	[DomainNameProviders.FLAP]: {
		value: DomainNameProviders.FLAP,
		favicon: "/logo.svg",
		authentication: DomainAuthenticationSchemes.Token,
	},
	[DomainNameProviders.Namecheap]: {
		value: DomainNameProviders.Namecheap,
		favicon:
			"https://external-content.duckduckgo.com/ip3/www.namecheap.com.ico",
		authentication: DomainAuthenticationSchemes.None,
	},
	[DomainNameProviders.DuckDns]: {
		value: DomainNameProviders.DuckDns,
		favicon:
			"https://external-content.duckduckgo.com/ip3/www.duckdns.org.ico",
		authentication: DomainAuthenticationSchemes.Token,
	},
	[DomainNameProviders.Local]: {
		value: DomainNameProviders.Local,
		favicon: "",
		authentication: DomainAuthenticationSchemes.None,
	},
	[DomainNameProviders.Localhost]: {
		value: DomainNameProviders.Localhost,
		favicon: "",
		authentication: DomainAuthenticationSchemes.None,
	},
	[DomainNameProviders.Unknown]: {
		value: DomainNameProviders.Unknown,
		favicon: "",
		authentication: DomainAuthenticationSchemes.None,
	},
}

export interface DomainNameInfo {
	name: string
	status: DomainStatus
	provider: DomainNameProviders
	authentication?: string
	username?: string
	logs: string
	dkim: { [key: string]: string | undefined }
}

export type DomainStatus = "OK" | "ERROR" | "HANDLED" | "WAITING"

export type VariableType = string | boolean
export type VariableTypeString = "string" | "boolean"
export type Variable = VariableInfo & {
	name: string
	value: VariableType
}
export type VariableInfo = {
	type: VariableTypeString
	info: string
	examples: string[]
	group: string
}
