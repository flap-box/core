const fs = require("fs")

process.env.VUE_APP_VERSION = process.env.GIT_TAG

module.exports = {
	configureWebpack: {
		devtool: "inline-source-map",
		devServer:
			process.env.NODE_ENV === "development"
				? {
						overlay: true,
						https: {
							key: fs.readFileSync(
								"/etc/letsencrypt/live/flap/privkey.pem",
							),
							cert: fs.readFileSync(
								"/etc/letsencrypt/live/flap/fullchain.pem",
							),
							ca: fs.readFileSync(
								"/etc/letsencrypt/live/flap/chain.pem",
							),
						},
						public: "flap.test:8081",
						writeToDisk: true,
				  }
				: {},
	},

	css: {
		loaderOptions: {
			sass: {
				additionalData: `@import "./src/styles/variables.scss";`,
			},
		},
	},

	pluginOptions: {
		i18n: {
			locale: "fr",
			fallbackLocale: "en",
			localeDir: "locales",
			enableInSFC: false,
		},
	},
}
