import * as fs from "fs-extra"

import {
	DomainNameInfo,
	DomainNameProviders,
	DomainStatus,
} from "../front/src/models"
import { logger, readFile } from "../tools"

export async function getDomains(): Promise<DomainNameInfo[]> {
	logger.debug("Reading domains info.")

	const domainNames = await fs.readdir("/var/lib/flap/data/domains")

	const domainsInfo = domainNames.map(async domainName => {
		try {
			return await getDomain(domainName)
		} catch (error) {
			logger.error(error)
			return {
				name: domainName,
				status: "ERROR" as DomainStatus,
				provider: DomainNameProviders.Unknown,
				authentication: "",
				logs: error.toString(),
				dkim: {},
			}
		}
	})

	return Promise.all(domainsInfo)
}

export async function getDomain(name: string): Promise<DomainNameInfo> {
	logger.silly(`Reading ${name} info`)

	return {
		name,
		status: (await readFile(
			`/var/lib/flap/data/domains/${name}/status.txt`,
		)) as DomainStatus,
		provider: (await readFile(
			`/var/lib/flap/data/domains/${name}/provider.txt`,
		)) as DomainNameProviders,
		authentication: await readFile(
			`/var/lib/flap/data/domains/${name}/authentication.txt`,
		),
		logs: await readFile(`/var/lib/flap/data/domains/${name}/logs.txt`),
		dkim: await readDKIM(name),
	}
}

async function readDKIM(name: string) {
	let dkimRecord
	try {
		dkimRecord = await readFile(`/var/lib/opendkim/${name}/mail.txt`)
	} catch (error) {
		return {}
	}

	const keysSearch = dkimRecord
		// Remove white spaces and quotes.
		.replace(/\r?\n|\r|\t| |"/g, "")
		// Match every a=...; groups.
		.match(/[a-z]=[a-zA-Z0-9\/\+]+;?/g)

	// Return en empty dkim if the match has failed.
	if (keysSearch === null) {
		return {}
	}

	// Build a dkim from the matches.
	return keysSearch
		.map(key => key.replace(";", "")) // a=value; => a=value
		.map(key => key.split("=")) // a=value => ['a', 'value']
		.map(([key, value]) => ({ [key]: value })) // ['a', 'value'] => { 'a': 'value' }
		.reduce((dkim, entry) => ({ ...dkim, ...entry })) // Merge all entries into one object.
}

export async function setDomain(domain: DomainNameInfo) {
	logger.info("Writing new domain info")

	// Try to read domain info for the domain we want to set.
	let existingDomain: DomainNameInfo | undefined = undefined
	try {
		existingDomain = await getDomain(domain.name)
	} catch (error) {
		// If an error is thrown, it means that the domain do not exists,
		// we can ignore it and continue.
	}

	if (existingDomain !== undefined) {
		throw new Error("The domain already exist, try to update it instead.")
	}

	// Write the domain
	await fs.mkdir(`/var/lib/flap/data/domains/${domain.name}`)
	await writeDomainInfo(domain)
}

export async function patchDomain(domain: DomainNameInfo) {
	logger.info("Writing patch domain info")

	// Try to read domain info for the domain we want to set.
	try {
		await getDomain(domain.name)
	} catch (error) {
		throw new Error("The domain does not exist, try to create it instead.")
	}

	if (domain.status === "HANDLED") {
		// If the domain is currently beeing handled, stop here.
		throw new Error(
			"Some operations related to the domain are pending. Try again later.",
		)
	}

	await writeDomainInfo(domain)
}

async function writeDomainInfo(domain: DomainNameInfo) {
	// Write the domain
	await fs.writeFile(
		`/var/lib/flap/data/domains/${domain.name}/status.txt`,
		"WAITING",
	)
	await fs.writeFile(
		`/var/lib/flap/data/domains/${domain.name}/provider.txt`,
		domain.provider,
	)
	await fs.writeFile(
		`/var/lib/flap/data/domains/${domain.name}/authentication.txt`,
		domain.authentication,
	)
	await fs.writeFile(
		`/var/lib/flap/data/domains/${domain.name}/username.txt`,
		domain.username,
	)
	await fs.writeFile(`/var/lib/flap/data/domains/${domain.name}/logs.txt`, "")
}

export async function deleteDomain(name: string) {
	logger.info(`Deletting ${name} info`)

	if (await isLocked("delete")) {
		throw new Error("A domain deletion update is already in progress")
	}

	try {
		await lock("delete")

		// Prevent deleting the primary domain.
		const primaryDomain = await getPrimaryDomain()
		if (
			primaryDomain.domain !== undefined &&
			primaryDomain.domain.name === name
		) {
			throw new Error("Can't delete the primary domain.")
		}

		// Prevent deleting a HANDLED domain.
		const domain = await getDomain(name)
		if (domain.status === "HANDLED") {
			throw new Error("Can't delete a HANDLED domain.")
		}

		// Prevent deleting the last OK domain.
		const domains = await getDomains()
		const okDomains = domains.filter(domain => domain.status === "OK")
		if (domain.status === "OK" && okDomains.length === 1) {
			throw new Error("Can't delete the last OK domain.")
		}

		// Delete the domain info.
		await fs.remove(`/var/lib/flap/data/domains/${name}`)
	} catch {
		await unLock("delete")
	}
}

export async function getPrimaryDomain(): Promise<{
	loading: boolean
	domain?: DomainNameInfo
}> {
	logger.debug("Reading primary domain.")

	// Try to read primary_domain.txt.
	let domain: DomainNameInfo | undefined
	try {
		const primary = await readFile(`/var/lib/flap/data/primary_domain.txt`)
		domain = await getDomain(primary)
	} catch (error) {
		logger.error(error)
	}

	logger.silly(`Primary domain is ${JSON.stringify(domain)}.`)
	return { loading: await isLocked("primary"), domain }
}

export async function setPrimaryDomain(name: string) {
	logger.info(`Setting ${name} as primary domain.`)

	if (await isLocked("primary")) {
		throw new Error("A domain primary update is already in progress")
	}

	try {
		lock("primary")

		// Prevent setting a non OK domain as primary.
		const domain = await getDomain(name)
		if (domain.status !== "OK") {
			throw new Error("Can't set a non OK domain as primary.")
		}

		await fs.writeFile("/var/lib/flap/data/primary_domain.txt", name)
	} catch {
		unLock("primary")
	}
}

// Helper to prevent concurent update requests.
async function lock(action: "delete" | "primary") {
	await fs.writeFile(`/var/lib/flap/data/domain_update_${action}.txt`, 1)
}

async function isLocked(action: "delete" | "primary") {
	try {
		await fs.stat(`/var/lib/flap/data/domain_update_${action}.txt`)
		return true
	} catch {
		return false
	}
}

async function unLock(action: "delete" | "primary") {
	await fs.remove(`/var/lib/flap/data/domain_update_${action}.txt`)
}
