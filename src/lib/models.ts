export interface IApp {
	name: string
	enabled: boolean
	subdomain: string
	icon: string
}
