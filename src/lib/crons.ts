import { searchUsers, updateUser } from "./users"

export async function updateMailAliases() {
	const users = await searchUsers()

	for (let user of users) {
		await updateUser(user.username, {})
	}
}
