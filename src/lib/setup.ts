import { promises as fs } from "fs"

import { searchUsers } from "./users"
import { getPrimaryDomain } from "./domains"
import { logger } from "../tools"

export async function getSetupProgress() {
	// Wait for installation to be done
	try {
		await fs.readFile("/var/lib/flap/data/installation_done.txt")
	} catch (error) {
		return -1
	}

	const usersList = await searchUsers()
	const primaryDomain = await getPrimaryDomain()

	logger.silly(`Setup step one: ${JSON.stringify(usersList)}`)
	logger.silly(`Setup step two: ${JSON.stringify(primaryDomain)}`)

	if (usersList.length <= 0) {
		// Step one is to create the first user
		return 0
	} else if (primaryDomain === undefined || primaryDomain.domain === undefined) {
		// Step two is to set the domain name
		return 1
	} else {
		// If here, both conditions are false and the setup is done
		return 2
	}
}
