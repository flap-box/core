# Home GUI for FLAP

---

This is the home GUI for FLAP.

### Screenshots

![Home page](https://gitlab.com/flap-box/core/raw/master/screenshots/home.png)
![Settings](https://gitlab.com/flap-box/core/raw/master/screenshots/settings.png)

### Functionality

-   Get setup status
-   Manage users
-   Manage domain names
-   List apps
-   Serve a web GUI

### Contributing

The project is composed of a NodeJS backend and a VueJS frontend. Both are written with Typescript.

##### Cloning

It is preferable to clone the main FLAP project:

`git clone --recursive git@gitlab.com:flap-box/flap.git`

We need the `--recursive` flag so submodules are also cloned.

### Possible improvements

-   Rewrite in a lighter language. Probably Go.
-   Find ways to make domain names requests more responsive.

##### Developing

-   Follow the main FLAP `README.md` to setup your workspace.
-   Install NodeJS.
-   Backend - `cd` into the `./flap/home` directory. - `npm install`.
-   Frontend - `cd` into the `./flap/home/src/front` directory. - `npm install`. - `npm run serve` will live compile the GUI. You can also use the `vue ui` if you have installed `vue-cli` globally with `npm install -g vue-cli`.
-   Start the containers: `sudo -E flapctl start`.

###### Resources

-   [NodeJS](https://nodejs.org/en/)
-   [ExpressJS](https://expressjs.com/)
-   [CodeceptJS](https://codecept.io/)
-   [NodeMailer](https://nodemailer.com/)

### REST API

| METHOD | Path                | Body                                                    | Response                                  | Action                  |
| ------ | ------------------- | ------------------------------------------------------- | ----------------------------------------- | ----------------------- |
| GET    | /apps               |                                                         | JSON list of apps accessible by the user. |
| GET    | /domains            | `[{name, provider, authentication, status, logs}, ...]` | Domain names setted for the box.          |                         |
| POST   | /domains            | `{name, provider, authentication}`                      | The new domain.                           | Create a new domain.    |
| DELETE | /domains/{domainId} |                                                         |                                           | Delete the domain name. |
| GET    | /domains/primary    |                                                         | The primary domain.                       |                         |
| PUT    | /domains/primary    | `{name}`                                                | The primary domain.                       | Set the primary domain. |
| GET    | /me                 | `{username, fullname, email, admin}`                    | The connected user                        |                         |
| GET    | /setup              | `{progress}`                                            | The setup progress `{0-2}`.               |                         |
| GET    | /users              | `[ {username, fullname, password, email, admin}, ... ]` | JSON list of users.                       |                         |
| POST   | /users              | `{username?, fullname, password?, email, admin}`        | The created user.                         | Create a new user.      |
| GET    | /users/{userId}     |                                                         | The user.                                 |                         |
| PATCH  | /users/{userId}     | `{fullname?, password?, email?, admin?}`                | The updated user.                         | Update the user.        |
| DELETE | /users/{userId}     |                                                         |                                           | Delete the user.        |
