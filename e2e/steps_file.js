// in this file you can append custom step methods to 'I' object

module.exports = function() {
	return actor({
		login: function(username, password, shouldFail = false) {
			this.amOnPage("/")
			this.fillField("user", username)
			this.fillField("password", password)
			this.click("Connect")

			if (shouldFail) {
				return
			}

			this.waitForNavigation({})
			this.seeInCurrentUrl(`https://home.${process.env.FLAP_URL}`)
		},

		logout: async function() {
			this.clearCookie()
			this.click("#logout-button")
			this.waitInUrl(`https://auth.${process.env.FLAP_URL}`)
		},
	})
}
