/// <reference path="../steps.d.ts" />

Feature("login")

Scenario("test login valid creds", I => {
	I.amOnPage("/")

	I.seeInCurrentUrl(`https://auth.${process.env.FLAP_URL}`)
	I.dontSeeCookie("flap-sso")
	I.see("Authentication required")

	I.login("theadmin", "password")

	I.seeInCurrentUrl(`https://home.${process.env.FLAP_URL}`)
	I.seeCookie("flap-sso")
	I.see("FLAP of Mr. Admin")
})

Scenario("test login wrong creds", I => {
	I.amOnPage("/")

	I.see("Authentication required")

	I.login("wronguser", "wrongpassword", true)

	I.see("Wrong credentials")
	I.dontSeeCookie("flap-sso")
	I.seeInCurrentUrl(`https://auth.${process.env.FLAP_URL}`)
})

Scenario("test logout", I => {
	I.amOnPage("/")

	I.login("theadmin", "password")
	I.logout()

	I.seeInCurrentUrl(`https://auth.${process.env.FLAP_URL}`)
})

Scenario("test login logout login", I => {
	I.amOnPage("/")

	I.login("theadmin", "password")
	I.logout()
	I.login("theadmin", "password")

	I.seeInCurrentUrl(`https://home.${process.env.FLAP_URL}`)
})
