/// <reference path="../steps.d.ts" />

const { I } = inject()

module.exports = {
	open(tab = "me") {
		I.click("#settings-link")
		I.waitInUrl(`https://home.${process.env.FLAP_URL}/settings`)
		I.waitForElement(`//li[@role='tab'][@aria-controls='${tab}']`)
		I.click(`//li[@role='tab'][@aria-controls='${tab}']`)
	},

	createUser(fullname, email, username, password) {
		I.click("Add a user")

		within(".user-creation-form", () => {
			I.fillField("fullname", fullname)
			I.fillField("email", email)
			I.seeInField("username", username)
			I.fillField("password", password)
			I.click("Create the new user")
			I.dontSeeElement(".ui-alert")
		})

		I.waitForElement(`.${username}`, 5)
		I.dontSee(".user-creation-form")
	},

	deleteUser(username) {
		I.seeElement(`.${username}`)

		within(`.${username}`, () => {
			I.click("Delete the user")
		})

		within(".ui-modal.is-open", () => {
			I.see("Do you really want to delete: user ?")
			I.click("delete")
		})

		I.dontSeeElement(`.${username}`)
	},

	updateMyInfo(fullname, email) {
		I.click("Update my information")

		I.waitForText("Profile modification")

		within("#profile-form", () => {
			I.clearField("fullname")
			I.fillField("fullname", fullname)
			I.clearField("email")
			I.fillField("email", email)
			I.click("update")
			I.dontSeeElement(".ui-alert")
		})

		I.waitForInvisible("#profile-form")
		I.dontSeeElement("#profile-form")
	},

	updateMypassword(oldpassword, newpassword) {
		I.click("Update my password")

		I.waitForText("Password modification")

		within("#password-form", () => {
			I.fillField("oldPassword", oldpassword)
			I.fillField("newPassword", newpassword)
			I.click("update")
			I.dontSeeElement(".ui-alert")
		})

		I.waitForInvisible("#password-form", 4)
		I.dontSeeElement("#password-form")
	},
}
