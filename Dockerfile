# BUILD STAGE
FROM node:12-buster-slim as builder
WORKDIR /home
# Build dependencies
RUN apt-get update
RUN apt-get install -y build-essential python
COPY ./ ./
# SERVER
RUN npm install
# Build server
RUN npm run build
# Reinstall node_module but with --only=production flag to not install all dependencies
# The generated node_modules will be used in the runtime stage
RUN rm -rf ./node_modules
RUN npm install --only=production
# FRONT
WORKDIR /home/src/front
RUN npm install
RUN npm run build

# RUNTIME STAGE
FROM node:12-buster-slim
EXPOSE 9000
WORKDIR /home
COPY --from=builder /home/build ./build
COPY --from=builder /home/package.json ./package.json
COPY --from=builder /home/node_modules ./node_modules
COPY --from=builder /home/src/front/dist ./src/front/dist
RUN apt-get update && \
    # Runtime dependencies
    apt-get install -y whois && \
    # Reduce final image size
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
CMD ["npm", "start"]
